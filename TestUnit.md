# HOW TO MAKE TEST ON JEST
```
// TESTER UNE FONCTION
    // APPELER LA FONCTION
    // VERIFIER QUELLE RENVOI BIEN LA BONNE CHOSE
    // SI ELLE MODIFIE DES VARIABLES GLOBALES / DE CLASSE
        // TESTER QUELLES SONT BIEN MODIFIEES
    // SI ELLE UTILISE DAUTRES FONCTIONS
        // ON REGARDE SIMPLEMENT QUELLES SONT APPELEES
        // ET ON LES MOCK/STUB/SPY POUR QU'ELLES RENVOIENT LES BONNES DONNEES
          // ON EST PAS LA POUR RECOMPILER TOUT LE PROGRAMME
          // NI POUR TESTER QUE CES AUTRES FONCTIONS FONCTIONNENT
```
```
// Command jest
npx jest --clear-cache
npx jest <NameFile> --no-cache
```
```JS
describe('Use-cases-user', () => {
  it.only('Should work', () => {
    const user = { isConnected: false, token: '' };
    const component = {
      callApi: (value: string) => {},
    };
    const connectUser = (token: string): boolean => {
      if (token == 'validToken') {
        user.isConnected = true;
        user.token = token;
        component.callApi(token);
        return true;
      } else {
        user.token = '';
        user.isConnected = false;
        return false;
      }
    };

    component.callApi = jest.fn();

    const isConnected = connectUser('validToken');
    expect(component.callApi).toBeCalledWith('validToken');
    expect(component.callApi).toBeCalledTimes(1);
    expect(isConnected).toBe(true);
    expect(user).toEqual({ isConnected: true, token: 'validToken' });

    (component.callApi as jest.Mock).mockClear();
    component.callApi = jest.fn();

    const isNotConnected = connectUser('invalidToken');
    expect(component.callApi).not.toBeCalled();
    expect(isNotConnected).toBe(false);
    expect(user).toEqual({ isConnected: false, token: '' });
  });
});
```