describe('Use-cases-user', () => {
  it.only('Should work', () => {
    const user = { isConnected: false, token: '' };
    const component = {
      callApi: (value?: string) => {
        return;
      },
    };
    const connectUser = (token: string): boolean => {
      if (token == 'validToken') {
        user.isConnected = true;
        user.token = token;
        component.callApi(token);
        return true;
      } else {
        user.token = '';
        user.isConnected = false;
        return false;
      }
    };

    component.callApi = jest.fn();

    const isConnected = connectUser('validToken');
    expect(component.callApi).toBeCalledWith('validToken');
    expect(component.callApi).toBeCalledTimes(1);
    expect(isConnected).toBe(true);
    expect(user).toEqual({ isConnected: true, token: 'validToken' });

    (component.callApi as jest.Mock).mockClear();
    component.callApi = jest.fn();

    const isNotConnected = connectUser('invalidToken');
    expect(component.callApi).not.toBeCalled();
    expect(isNotConnected).toBe(false);
    expect(user).toEqual({ isConnected: false, token: '' });
  });
});

// it('Should save user', async () => {
//   /**
//    * Spy on the model save function and return a completed promise.
//    * We are not testing the model here, only the controller so this is ok.
//    */
//   jest.spyOn(Foo.prototype, 'save').mockImplementationOnce(() => Promise.resolve());

//   /**
//    * Create a mock request and set type to any to tell typescript to ignore type checking
//    */
//   const mockRequest: any = {
//     user: {
//       _id: 1234,
//     },
//     body: {
//       bars: ['baz', 'qux', 'quux'],
//     },
//   };

//   /**
//    * Create a mock repsonse with only the methods that are called in the controller and
//    * record their output with jest.fn()
//    */
//   const mockResponse: any = {
//     status: jest.fn(),
//     json: jest.fn(),
//   };

//   /**
//    * Create a mock next function. It is okay to set its type to Express's NextFunction because
//    * we are mocking the entire function.
//    */
//   const mockNext: NextFunction = jest.fn();

//   await fooController.createFoo(mockRequest, mockResponse, mockNext);

//   expect(mockResponse.json).toHaveBeenCalledTimes(1);
//   expect(mockResponse.json).toHaveBeenCalledWith('bars');

//   expect(mockResponse.status).toHaveBeenCalledTimes(1);
//   expect(mockResponse.status).toHaveBeenCalledWith(200);
// });

// mockingoose(UserModel).toReturn({ motoToCreate }, 'save');

// const userSave: Partial<IUser> = await userControllerTest.create(motoToCreate);
// expect(JSON.stringify(motoToProfil)).toEqual(JSON.stringify(userSave));

// mockingoose(UserModel).reset();
