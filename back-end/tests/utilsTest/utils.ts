import { NextFunction, Request, Response } from 'express';

export const customErrorTest = (value: string): Error => {
  return new Error(value);
};

const mockRequestBody = (valueBody: any): Partial<Request> => {
  return {
    body: valueBody,
  };
};
const mockRequestOther = (valueBody: any): Partial<Request> => {
  return valueBody;
};
const mockResponse: Partial<Response> = {
  status: jest.fn(),
  send: jest.fn(),
  json: jest.fn(),
};

const mockNext: Partial<NextFunction> = {
  next: jest.fn(),
};

export const mockMiddleware = {
  mockRequestBody,
  mockResponse,
  mockNext,
  mockRequestOther,
};
