import { UserModel } from '../../src/entities/types/user/User.schema';

const motoToProfil: any = {
  _id: '5fd3e54d7d88981a74149f24',
  avatar: null,
  firstName: 'moto',
  lastName: 'moto',
};

const motoToCreate: any = {
  _id: '5fd3e54d7d88981a74149f24',
  email: 'mototest@yahoo.fr',
  password: 'moto',
  firstName: 'moto',
  lastName: 'moto',
  birthDate: '2000-05-10',
  description: 'fhfdsfs,fp,fd,,sfdlfl,fds,a;dpspp;deo',
};
const motoToCreateHashPass: any = {
  email: 'mototest@yahoo.fr',
  password: '7dbb8b144228e2f24fe4279cb204bedbf97914cff022a18a1e73ddfd49b310c4',
  firstName: 'moto',
  lastName: 'moto',
  birthDate: '2000-05-10',
  description: 'fhfdsfs,fp,fd,,sfdlfl,fds,a;dpspp;deo',
};

const motoCreated: any = {
  _id: '5fd3e54d7d88981a74149f24',
  email: 'mototest@yahoo.fr',
  password: '7dbb8b144228e2f24fe4279cb204bedbf97914cff022a18a1e73ddfd49b310c4',
  firstName: 'moto',
  lastName: 'moto',
  birthDate: new Date('2000-05-10T00:00:00.000Z'),
  createdAt: new Date('2020-12-11T21:31:57.098Z'),
  updatedAt: new Date('2020-12-11T21:31:57.098Z'),
  __v: 0,
};

const titiCreated: any = {
  _id: '5fd3e54d7d88981a74149f25',
  email: 'titi@yahoo.fr',
  password: '7dbb8b144228e2f24fe4279cb204bedbf97914cff022a18a1e73ddfd49b310c4',
  firstName: 'titi',
  lastName: 'titi',
  birthDate: new Date('2000-05-10T00:00:00.000Z'),
  createdAt: new Date('2020-12-11T21:31:57.098Z'),
  updatedAt: new Date('2020-12-11T21:31:57.098Z'),
  __v: 0,
};

const motoModel: any = {
  _id: '5fd3e54d7d88981a74149f24',
  email: 'mototest@yahoo.fr',
  password: 'moto',
  firstName: 'moto',
  lastName: 'moto',
  birthDate: '2000-05-10',
  description: 'fhfdsfs,fp,fd,,sfdlfl,fds,a;dpspp;deo',
};

export { motoToProfil, motoCreated, motoToCreate, motoToCreateHashPass, motoModel, titiCreated };
