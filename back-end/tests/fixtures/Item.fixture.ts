import { UserModel } from '../../src/entities/types/user/User.schema';
import { motoCreated } from './User.fixture';

const porteToCreated: any = {
  title: 'Porte en bois',
  price: 50,
  amount: 1500,
  shortDescription: 'Une porte en bois pour la garage',
  completeDescription: 'Une porte en bois pour la garage et pour les maisons',
  articlePersonalize: false,
  user: motoCreated,
  images: [
    { name: 'Porte en bois classique', url: 'imagesdeporte.jng' },
    { name: 'Porte en pvc ', url: 'imagesdeportepvc.jng' },
  ],
};
const porteCreated: any = {
  _id: '5ff6bd3c51f00440500b85d9',
  title: 'Porte en bois',
  price: 50,
  amount: 1500,
  shortDescription: 'Une porte en bois pour la garage',
  completeDescription: 'Une porte en bois pour la garage et pour les maisons',
  articlePersonalize: false,
  user: motoCreated,
  images: [
    { name: 'Porte en bois classique', url: 'imagesdeporte.jng' },
    { name: 'Porte en pvc ', url: 'imagesdeportepvc.jng' },
  ],
  createdAt: new Date('2020-12-11T21:31:57.098Z'),
};
const porteInfo: any = {
  _id: '5ff6bd3c51f00440500b85d9',
  title: 'Porte en bois',
  price: 50,
  shortDescription: 'Une porte en bois pour la garage',
  averageMark: null,
  images: [
    { name: 'Porte en bois classique', url: 'imagesdeporte.jng' },
    { name: 'Porte en pvc ', url: 'imagesdeportepvc.jng' },
  ],
};
const tshirtToCreated: any = {
  title: 'T-shirt de manga',
  price: 35,
  amount: 3000,
  shortDescription: 'T-shirt personnalisable avec le logo de ton choix',
  completeDescription: 'T-shirt personnalisable avec le logo de ton choix comme par exemple one piece, nartuo...',
  articlePersonalize: true,
  user: motoCreated,
  images: [
    { name: 'Tshirt de one piece', url: 'onepiece.jng' },
    { name: 'Tshirt de bleach', url: 'naruto.jng' },
    { name: 'Tshirt de bleach ', url: 'bleach.jng' },
  ],
};
const tshirtCreated: any = {
  _id: '5ff6bd3c51f00440500b85d6',
  title: 'T-shirt de manga',
  price: 35,
  amount: 3000,
  shortDescription: 'T-shirt personnalisable avec le logo de ton choix',
  completeDescription: 'T-shirt personnalisable avec le logo de ton choix comme par exemple one piece, nartuo...',
  articlePersonalize: true,
  user: motoCreated,
  images: [
    { name: 'Tshirt de one piece', url: 'onepiece.jng' },
    { name: 'Tshirt de bleach', url: 'naruto.jng' },
    { name: 'Tshirt de bleach ', url: 'bleach.jng' },
  ],
  createdAt: new Date('2020-8-11T21:31:57.098Z'),
};
const tshirtInfo: any = {
  _id: '5ff6bd3c51f00440500b85d6',
  title: 'T-shirt de manga',
  price: 35,
  shortDescription: 'T-shirt personnalisable avec le logo de ton choix',
  averageMark: null,
  images: [
    { name: 'Tshirt de one piece', url: 'onepiece.jng' },
    { name: 'Tshirt de bleach', url: 'naruto.jng' },
    { name: 'Tshirt de bleach ', url: 'bleach.jng' },
  ],
};

export { porteToCreated, porteCreated, porteInfo, tshirtToCreated, tshirtCreated, tshirtInfo };
