const someToken: any = {
  token:
    'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVmOGMyY2RlNDU4MDg3MjFlMDY3YWY1ZCIsImxhc3ROYW1lIjoiRGVzZnJhbsOnYWlzIiwiZmlyc3ROYW1lIjoiRnJhbmNrIiwiaWF0IjoxNjAzMDI0ODU3fQ.29AeU7uq7Zy4xX6o4x92i73RiZQfKu9gfgt2JG2BnXE',
};
const someTokenMoto: any = {
  token:
    'eyJhbGciOiJIUzI1NiJ9.IjIwMzI4YTBkOTUzNTJmZWZjN2UyYWJlZjU3YjAyMjU2OTM1ZjQ2YmNhMTQwYjRjZmVlMGIwNTFhMWFiYmE1NjEi.VN8SJXWL-ULo0xRe4NgwmMPnJ0_1AOA8KKmXDxNttPU',
};
export { someToken, someTokenMoto };
