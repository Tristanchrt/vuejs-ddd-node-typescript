import { UserImplementation } from '../../../../src/implementation/user/User';
import { NextFunction, Request, Response } from 'express';
import { motoCreated, motoToCreate, motoToProfil, titiCreated } from '../../../fixtures/User.fixture';
import sinon from 'sinon';
import { customErrorTest, mockMiddleware } from '../../../utilsTest/utils';
import { someTokenMoto } from '../../../fixtures/Utils.fixture';
import { tokenFunction } from '../../../../src/utils/token';

describe('UserImplemenation', () => {
  class UserImplementationTest extends UserImplementation {}
  const userImplementationTest: UserImplementationTest = new UserImplementationTest();

  beforeEach(() => {
    jest.restoreAllMocks();
  });

  it('Should check identity of the user with token', async () => {
    userImplementationTest.checkIdentity = jest.fn().mockImplementationOnce(() => Promise.resolve(motoToProfil));
    tokenFunction.verifToken = jest.fn().mockReturnValue(true);

    const next = jest.fn();

    await userImplementationTest.checkIdentityImplement(
      mockMiddleware.mockRequestOther({
        headers: { authorization: someTokenMoto },
      }) as Request,
      mockMiddleware.mockResponse as Response,
      next as NextFunction,
    );

    expect(next).toBeCalled();
  });
  it('Should not check identity of the user with token', async () => {
    userImplementationTest.checkIdentity = jest
      .fn()
      .mockImplementationOnce(() => Promise.reject(customErrorTest('Error for checking identity')));
    tokenFunction.verifToken = jest.fn().mockReturnValue(true);

    const next = jest.fn();

    await userImplementationTest.checkIdentityImplement(
      mockMiddleware.mockRequestOther({
        headers: { authorization: someTokenMoto },
      }) as Request,
      mockMiddleware.mockResponse as Response,
      next as NextFunction,
    );

    expect(next).not.toBeCalled();
    expect(mockMiddleware.mockResponse.status).toBeCalledWith(404);
    expect(mockMiddleware.mockResponse.json).toBeCalledWith(customErrorTest('Error for checking identity'));
    try {
      await userImplementationTest.checkIdentityImplement(
        mockMiddleware.mockRequestOther({}) as Request,
        mockMiddleware.mockResponse as Response,
        next as NextFunction,
      );
    } catch (error) {
      expect(mockMiddleware.mockResponse.status).toBeCalledWith(404);
      expect(mockMiddleware.mockResponse.json).toBeCalledWith(customErrorTest('request prohibited'));
    }
    tokenFunction.verifToken = jest.fn().mockReturnValue(false);

    await userImplementationTest.checkIdentityImplement(
      mockMiddleware.mockRequestOther({ headers: { authorization: 'someToken' } }) as Request,
      mockMiddleware.mockResponse as Response,
      next as NextFunction,
    );
    expect(next).not.toBeCalled();
    expect(mockMiddleware.mockResponse.status).toBeCalledWith(404);
    expect(mockMiddleware.mockResponse.json).toBeCalledWith('request prohibited');
  });

  it('Should connect user with request status 200', async () => {
    userImplementationTest.connect = jest.fn().mockImplementationOnce(() => Promise.resolve(motoToProfil));

    await userImplementationTest.connectImplement(
      mockMiddleware.mockRequestBody({
        email: 'someEmail',
        password: 'somePassword',
      }) as Request,
      mockMiddleware.mockResponse as Response,
    );
    expect(userImplementationTest.connect).toHaveBeenCalledWith('someEmail', 'somePassword');
    expect(mockMiddleware.mockResponse.json).toBeCalledWith(motoToProfil);
    expect(mockMiddleware.mockResponse.status).toBeCalledWith(200);
  });

  it('Shoud not connect user with request status 404 ', async () => {
    userImplementationTest.connect = jest
      .fn()
      .mockImplementationOnce(() => Promise.reject(customErrorTest('Error during connection')));

    await userImplementationTest.connectImplement(
      mockMiddleware.mockRequestBody({
        email: 'someEmail',
        password: 'somePassword',
      }) as Request,
      mockMiddleware.mockResponse as Response,
    );
    expect(mockMiddleware.mockResponse.status).toBeCalledWith(404);
    expect(mockMiddleware.mockResponse.json).toBeCalledWith(customErrorTest('Error during connection'));
  });

  it('Should send an express response to the client with all users', async () => {
    userImplementationTest.findAll = jest
      .fn()
      .mockImplementationOnce(() => Promise.resolve([titiCreated, motoCreated]));

    await userImplementationTest.findAllImplement(
      mockMiddleware.mockRequestBody({}) as Request,
      mockMiddleware.mockResponse as Response,
    );
    expect(userImplementationTest.findAll).toBeCalled();
    expect(mockMiddleware.mockResponse.json).toBeCalledWith([titiCreated, motoCreated]);
    expect(mockMiddleware.mockResponse.status).toBeCalledWith(200);
  });
  it('Should not send an express response to the client with all users', async () => {
    userImplementationTest.findAll = jest
      .fn()
      .mockImplementationOnce(() => Promise.reject(customErrorTest('Error for finding all users')));

    await userImplementationTest.findAllImplement(
      mockMiddleware.mockRequestBody({}) as Request,
      mockMiddleware.mockResponse as Response,
    );
    expect(mockMiddleware.mockResponse.status).toBeCalledWith(404);
    expect(mockMiddleware.mockResponse.json).toBeCalledWith(customErrorTest('Error for finding all users'));
  });
  it('Should deleted one of user', async () => {
    userImplementationTest.deleteOne = jest.fn().mockImplementationOnce(() => Promise.resolve(motoToCreate));
    await userImplementationTest.deletOneImplement(
      mockMiddleware.mockRequestOther({ params: { id: motoToCreate._id } }) as Request,
      mockMiddleware.mockResponse as Response,
    );
    expect(userImplementationTest.deleteOne).toHaveBeenCalledWith(motoToCreate._id);
    expect(mockMiddleware.mockResponse.json).toBeCalledWith(motoToCreate);
    expect(mockMiddleware.mockResponse.status).toBeCalledWith(200);
  });
  it('Should not deleted one of user', async () => {
    userImplementationTest.deleteOne = jest
      .fn()
      .mockImplementationOnce(() => Promise.reject(customErrorTest('Error to deleted one of the user')));
    await userImplementationTest.deletOneImplement(
      mockMiddleware.mockRequestOther({ params: {} }) as Request,
      mockMiddleware.mockResponse as Response,
    );
    expect(mockMiddleware.mockResponse.status).toBeCalledWith(404);
    expect(mockMiddleware.mockResponse.json).toBeCalledWith(customErrorTest('Error to deleted one of the user'));
  });
  it('Should created user', async () => {
    userImplementationTest.create = jest.fn().mockImplementationOnce(() => Promise.resolve(motoToCreate));
    await userImplementationTest.createImplement(
      mockMiddleware.mockRequestBody(motoToCreate) as Request,
      mockMiddleware.mockResponse as Response,
    );
    expect(userImplementationTest.create).toHaveBeenCalledWith(motoToCreate);
    expect(mockMiddleware.mockResponse.json).toBeCalledWith(motoToCreate);
    expect(mockMiddleware.mockResponse.status).toBeCalledWith(201);
  });
  it('Should not created user', async () => {
    userImplementationTest.create = jest
      .fn()
      .mockImplementationOnce(() => Promise.reject(customErrorTest('Error for created user')));
    await userImplementationTest.createImplement(
      mockMiddleware.mockRequestBody({}) as Request,
      mockMiddleware.mockResponse as Response,
    );
    expect(mockMiddleware.mockResponse.json).toBeCalledWith(customErrorTest('Error for created user'));
    expect(mockMiddleware.mockResponse.status).toBeCalledWith(404);
  });
  it('Should find one of user', async () => {
    userImplementationTest.findOne = jest.fn().mockImplementationOnce(() => Promise.resolve(motoCreated));
    await userImplementationTest.findOneImplement(
      mockMiddleware.mockRequestOther({ params: { id: motoCreated._id } }) as Request,
      mockMiddleware.mockResponse as Response,
    );
    expect(userImplementationTest.findOne).toHaveBeenCalledWith(motoCreated._id);
    expect(mockMiddleware.mockResponse.status).toBeCalledWith(200);
    expect(mockMiddleware.mockResponse.json).toBeCalledWith(motoCreated);
  });
  it('Should not find one of user', async () => {
    userImplementationTest.findOne = jest
      .fn()
      .mockImplementationOnce(() => Promise.reject(customErrorTest('Error to find one of the user')));
    await userImplementationTest.findOneImplement(
      mockMiddleware.mockRequestOther({ params: { id: motoCreated._id } }) as Request,
      mockMiddleware.mockResponse as Response,
    );
    expect(mockMiddleware.mockResponse.status).toBeCalledWith(404);
    expect(mockMiddleware.mockResponse.json).toBeCalledWith(customErrorTest('Error to find one of the user'));

    try {
      await userImplementationTest.findOneImplement(
        mockMiddleware.mockRequestOther({ params: { id: motoCreated._id } }) as Request,
        mockMiddleware.mockResponse as Response,
      );
    } catch (error) {
      expect(mockMiddleware.mockResponse.status).toBeCalledWith(404);
      expect(error).toEqual('Error for find user');
    }
  });
});
