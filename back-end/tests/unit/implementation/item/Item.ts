import { ItemImplementation } from '../../../../src/implementation/item/item';
import { customErrorTest, mockMiddleware } from '../../../utilsTest/utils';
import { Request, Response } from 'express';
import { porteCreated, tshirtToCreated } from '../../../fixtures/Item.fixture';

describe('itemImplemenation', () => {
  class ItemImplementationTest extends ItemImplementation {}
  const itemImplementationTest: ItemImplementation = new ItemImplementationTest();

  beforeEach(() => {
    jest.restoreAllMocks();
  });

  it('Should not find all items and return express response', async () => {
    itemImplementationTest.findAll = jest
      .fn()
      .mockImplementationOnce(() => Promise.reject(customErrorTest('Error for finding all items')));

    await itemImplementationTest.findAllImplement(
      mockMiddleware.mockRequestBody({}) as Request,
      mockMiddleware.mockResponse as Response,
    );
    expect(mockMiddleware.mockResponse.status).toBeCalledWith(404);
    expect(mockMiddleware.mockResponse.json).toBeCalledWith(customErrorTest('Error for finding all items'));
  });
  it('Should find all items and return express response', async () => {
    itemImplementationTest.findAll = jest
      .fn()
      .mockImplementationOnce(() => Promise.resolve([tshirtToCreated, porteCreated]));

    await itemImplementationTest.findAllImplement(
      mockMiddleware.mockRequestBody({}) as Request,
      mockMiddleware.mockResponse as Response,
    );
    expect(itemImplementationTest.findAll).toBeCalled();
    expect(mockMiddleware.mockResponse.json).toBeCalledWith([tshirtToCreated, porteCreated]);
    expect(mockMiddleware.mockResponse.status).toBeCalledWith(200);
  });
  it('Should deleted one of item', async () => {
    itemImplementationTest.deleteOne = jest.fn().mockImplementationOnce(() => Promise.resolve(tshirtToCreated));
    await itemImplementationTest.deletOneImplement(
      mockMiddleware.mockRequestOther({ params: { id: tshirtToCreated._id } }) as Request,
      mockMiddleware.mockResponse as Response,
    );
    expect(itemImplementationTest.deleteOne).toHaveBeenCalledWith(tshirtToCreated._id);
    expect(mockMiddleware.mockResponse.json).toBeCalledWith(tshirtToCreated);
    expect(mockMiddleware.mockResponse.status).toBeCalledWith(200);
  });
  it('Should not deleted one of item', async () => {
    itemImplementationTest.deleteOne = jest
      .fn()
      .mockImplementationOnce(() => Promise.reject(customErrorTest('Error to deleted one of the item')));
    await itemImplementationTest.deletOneImplement(
      mockMiddleware.mockRequestOther({ params: {} }) as Request,
      mockMiddleware.mockResponse as Response,
    );
    expect(mockMiddleware.mockResponse.status).toBeCalledWith(404);
    expect(mockMiddleware.mockResponse.json).toBeCalledWith(customErrorTest('Error to deleted one of the item'));
  });
  it('Should find one of item', async () => {
    itemImplementationTest.findOne = jest.fn().mockImplementationOnce(() => Promise.resolve(tshirtToCreated));
    await itemImplementationTest.findOneImplement(
      mockMiddleware.mockRequestOther({ params: { id: tshirtToCreated._id } }) as Request,
      mockMiddleware.mockResponse as Response,
    );
    expect(itemImplementationTest.findOne).toHaveBeenCalledWith(tshirtToCreated._id);
    expect(mockMiddleware.mockResponse.status).toBeCalledWith(200);
    expect(mockMiddleware.mockResponse.json).toBeCalledWith(tshirtToCreated);
  });
  it('Should not find one of item', async () => {
    itemImplementationTest.findOne = jest
      .fn()
      .mockImplementationOnce(() => Promise.reject(customErrorTest('Error to find one of the item')));
    await itemImplementationTest.findOneImplement(
      mockMiddleware.mockRequestOther({ params: { id: tshirtToCreated._id } }) as Request,
      mockMiddleware.mockResponse as Response,
    );
    expect(mockMiddleware.mockResponse.status).toBeCalledWith(404);
    expect(mockMiddleware.mockResponse.json).toBeCalledWith(customErrorTest('Error to find one of the item'));

    try {
      await itemImplementationTest.findOneImplement(
        mockMiddleware.mockRequestOther({ params: { id: tshirtToCreated._id } }) as Request,
        mockMiddleware.mockResponse as Response,
      );
    } catch (error) {
      expect(mockMiddleware.mockResponse.status).toBeCalledWith(404);
      expect(error).toEqual('Error for find item');
    }
  });
});
