import { customError } from '../../../src/utils/function';
import { tokenFunction } from '../../../src/utils/token';
import jwt from 'jsonwebtoken';

describe('Utils ', () => {
  beforeEach(() => {
    jest.restoreAllMocks();
  });

  it('Should send an error', async () => {
    try {
      customError('Error test');
    } catch (error) {
      expect(error).toEqual(new Error('Error test'));
    }
  });
  it('Should verif token if not return false', () => {
    jwt.verify = jest.fn().mockReturnValueOnce(false);
    expect(tokenFunction.verifToken('someToken')).toBeFalsy();
  });
  it('Should verif token if not return false', () => {
    jwt.verify = jest.fn().mockImplementation(() => {
      throw new Error('');
    });
    try {
      const isCreated = tokenFunction.verifToken('');
    } catch (error) {
      expect(error).toEqual(false);
    }
  });
  it('Should verif token if valid return true', () => {
    jwt.verify = jest.fn().mockReturnValueOnce(true);
    expect(tokenFunction.verifToken('someToken')).toBeTruthy();
  });
  it('Should create token', () => {
    jwt.sign = jest.fn().mockReturnValueOnce('someToken');
    expect(tokenFunction.createToken('someToken')).toEqual('someToken');
  });
  it('Should create not token', async () => {
    try {
      jwt.sign = jest.fn().mockImplementation(() => {
        throw new Error('');
      });
      tokenFunction.createToken('someToken');
    } catch (error) {
      expect(error).toEqual(null);
    }
  });
});
