import { ItemModel } from '../../../../../src/entities/types/item/Item.schema';
import { ItemController } from '../../../../../src/controller/use-cases/item/Item';
import { porteCreated, porteInfo, tshirtCreated, tshirtInfo, tshirtToCreated } from '../../../../fixtures/Item.fixture';
import mockingoose from 'mockingoose';
import { IItem } from '../../../../../src/entities/types/item/Item.int';
import sinon from 'sinon';
import mongoose, { Document } from 'mongoose';
import rewire from 'rewire';
const mongonStub = rewire('mongoose');

describe('ItemUserCases', () => {
  class ItemControllerTest extends ItemController {}
  const itemControllerTest: ItemControllerTest = new ItemControllerTest();

  afterEach(async () => {
    jest.restoreAllMocks();
    mockingoose.resetAll();
  });

  it('Should not find one item', async () => {
    await expect(itemControllerTest.findOne('some-id')).rejects.toBe('Error during creation item');
  });
  it('Should find item', async () => {
    mockingoose(ItemModel).toReturn(porteCreated, 'findOne');

    await itemControllerTest.findOne('some-id').then((item) => {
      expect(JSON.stringify(item)).toEqual(JSON.stringify(porteInfo));
    });
    mockingoose(ItemModel).reset('findOne');
  });
  it('Should not find all items', async () => {
    const customError = 'Error to find all items';
    mockingoose(ItemModel).toReturn(customError, 'find');

    await itemControllerTest.findAll().catch((error) => {
      expect(error).toEqual(customError);
    });
  });
  it('Should find all items', async () => {
    mockingoose(ItemModel).toReturn([porteCreated, tshirtCreated], 'find');
    const items: Array<Partial<IItem>> = await itemControllerTest.findAll();
    expect(JSON.stringify(items[0]._id)).toEqual(JSON.stringify(porteCreated._id));
    expect(JSON.stringify(items[1]._id)).toEqual(JSON.stringify(tshirtCreated._id));
    mockingoose(ItemModel).reset('find');
  });
  it('Should delete one item', async () => {
    mockingoose(ItemModel).toReturn(porteCreated, 'deleteOne');
    const itemDeleted: Partial<IItem> = await itemControllerTest.deleteOne(porteCreated._id);
    expect(JSON.stringify(porteCreated._id)).toEqual(JSON.stringify(itemDeleted._id));
    mockingoose(ItemModel).reset('deleteOne');
  });
  it('Should not delete user', async () => {
    const customError = 'Error to delete item';
    mockingoose(ItemModel).toReturn(customError, 'deleteOne');

    await itemControllerTest.deleteOne('some-id').catch((error) => {
      expect(error).toEqual(customError);
    });
  });
});
