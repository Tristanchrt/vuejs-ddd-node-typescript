import mongoose, { Document } from 'mongoose';
import { motoCreated, motoToCreate, motoToProfil, titiCreated } from '../../../../fixtures/User.fixture';
import { UserController } from '../../../../../src/controller/use-cases/user/User';
import { IUser } from '../../../../../src/entities/types/user/User.int';
import { UserModel } from '../../../../../src/entities/types/user/User.schema';
import mockingoose from 'mockingoose';
import { someToken, someTokenMoto } from '../../../../fixtures/Utils.fixture';
import { tokenFunction } from '../../../../../src/utils/token';

describe('UserUserCases', () => {
  class UserControllerTest extends UserController {}
  const url = 'mongodb+srv://root:root@cluster0.sgqw3.gcp.mongodb.net/jest_db';
  let connection: mongoose.Connection;
  const userControllerTest: UserControllerTest = new UserControllerTest();

  beforeAll(async () => {
    mongoose.connect(url, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    const connection = mongoose.connection;

    connection.once('open', () => {
      console.log(`Connected to Mongoose Server test successfully`);
    });
  });
  afterAll(async () => {
    await connection.close();
  });
  afterEach(async () => {
    await UserModel.deleteMany({});
    jest.restoreAllMocks();
  });

  it('Should not find user', async () => {
    await expect(userControllerTest.findOne('some-id')).rejects.toBe('Error for finding user by id');
  });
  it('Should find one user', async () => {
    mockingoose(UserModel).toReturn(motoCreated, 'findOne');
    const userSave: Partial<IUser> = await userControllerTest.create(motoToCreate);
    expect(JSON.stringify(motoToProfil)).toEqual(JSON.stringify(userSave));
    mockingoose(UserModel).reset('find');
  });
  it('Should not save user in database', async () => {
    const customError = 'Error during creation user';
    try {
      await userControllerTest.create(motoToCreate);
      await userControllerTest.create(motoToCreate);
    } catch (error) {
      expect(error).toEqual(customError);
    }
    await expect(userControllerTest.create({ name: 'invalidData', email: 'invalidData' })).rejects.toBe(customError);
  });
  it('Should save user in database', async () => {
    const saveDB = jest.spyOn(UserModel.prototype, 'save');
    const userSave: Partial<IUser> = await userControllerTest.create(motoToCreate);

    expect(JSON.stringify(motoToProfil)).toEqual(JSON.stringify(userSave));

    expect(saveDB).toBeCalledWith();

    const insertedUser = await (await UserModel.findOne({ _id: userSave._id })).profile();
    expect(insertedUser).toEqual(userSave);
  });
  it('Should delete one user', async () => {
    mockingoose(UserModel).toReturn(motoCreated, 'deleteOne');
    const userDeleted: Partial<IUser> = await userControllerTest.deleteOne(motoToCreate._id);
    expect(JSON.stringify(motoCreated)).toEqual(JSON.stringify(userDeleted));
    mockingoose(UserModel).reset('deleteOne');
  });
  it('Should not delete user', async () => {
    const customError = 'Error to delete user';
    mockingoose(UserModel).toReturn(customError, 'deleteOne');

    await userControllerTest.deleteOne('some-id').catch((error) => {
      expect(error).toEqual(customError);
    });
  });
  it('Should find all users', async () => {
    mockingoose(UserModel).toReturn([titiCreated, motoCreated], 'find');

    const users: Array<Partial<IUser>> = await userControllerTest.findAll();
    expect(JSON.stringify(users)).toEqual(JSON.stringify([titiCreated, motoCreated]));
    mockingoose(UserModel).reset('find');
  });
  it('Should not find all users', async () => {
    const customError = 'Error to find all users';
    mockingoose(UserModel).toReturn(customError, 'find');

    await userControllerTest.findAll().catch((error) => {
      expect(error).toEqual(customError);
    });
  });
  it('Should not check identity', async () => {
    const customError = 'Error during check identidy';
    await expect(userControllerTest.checkIdentity('someToken')).rejects.toBe(customError);
    await expect(userControllerTest.checkIdentity(someToken.token)).rejects.toBe(customError);
  });
  it('Should check identity', async () => {
    mockingoose(UserModel).toReturn([titiCreated, motoCreated], 'find');

    const user: any = await userControllerTest.checkIdentity(someTokenMoto.token);
    expect(JSON.stringify(user)).toEqual(JSON.stringify(motoToProfil));
  });
  it('Should connect user by returning a token', async () => {
    mockingoose(UserModel).toReturn(motoCreated, 'findOne');

    tokenFunction.createToken = jest.fn().mockReturnValueOnce(someTokenMoto);

    const user: any = await userControllerTest.connect('someEmail', 'moto');

    expect(JSON.stringify(user.token)).toEqual(JSON.stringify(someTokenMoto));
  });
  it('Should not connect user by returning a token', async () => {
    const customError = 'Error during connection';
    await expect(userControllerTest.connect('someEmail', 'somePassword')).rejects.toBe(customError);
  });
});
