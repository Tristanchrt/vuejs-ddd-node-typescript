module.exports = {
  roots: ['<rootDir>/'],
  transform: {
    '^.+\\.tsx?$': 'ts-jest',
  },
  testEnvironment: 'node',
  testMatch: ['**/tests/unit/**/*.[jt]s?(x)', '**/?(*.)+(spec|test).[jt]s?(x)'],
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
  collectCoverage: true,
  reporters: [
    'default',
    [
      'jest-html-reporters',
      {
        // set a default repository
        publicPath: './jest',
        // rename the reporter file to reporter.html
        filename: 'reporter.html',
        // expand all tests sections in the reporter.html file
        expand: 'true',
      },
    ],
  ],
  // if set to false, coverage tests won't be run
  collectCoverage: true,
  coverageThreshold: {
    global: {
      statements: 100,
      branches: 100,
      functions: 100,
      lines: 100,
    },
  },
  collectCoverageFrom: [
    'src/**/*.{js,ts}',
    '!src/index.ts',
    '!src/routes/**/*.{js,ts}',
    '!src/entities/**/*.{js,ts}',
    '!tests/fixtures/**/*.{js,ts}',
    '!**/node_modules/**',
    '!src/server/**/*.{js,ts}',
    '!src/uploadFile/**/*.{file}',
  ],
  coverageReporters: ['html', 'json-summary', 'text-summary', 'lcov', 'clover'],
  // set a default repository
  coverageDirectory: './jest',
};
