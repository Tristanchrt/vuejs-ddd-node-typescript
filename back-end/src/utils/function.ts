import { sha256 } from 'js-sha256';
import { IUser } from '../entities/types/user/User.int';

export const hashVal = (valHash: any): string => {
  return sha256(valHash);
};

export const customError = (message: string): Error => {
  throw new Error(message);
};
