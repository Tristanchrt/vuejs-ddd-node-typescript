import atob from 'atob';
import jwt from 'jsonwebtoken';
import { IUser } from '../entities/types/user/User.int';
import { hashVal } from './function';

export const getTokenId = (token: string): string => {
  try {
    return JSON.parse(atob(token.split('.')[1]));
  } catch (err) {
    return null;
  }
};
const verifToken = (authorization: string): boolean => {
  try {
    const verif: string | unknown = jwt.verify(authorization, process.env.JWT_TOKEN);
    if (!verif) return false;
    return true;
  } catch (err) {
    return false;
  }
};
const createToken = (data: any): string => {
  try {
    return jwt.sign(JSON.stringify(hashVal(data)), process.env.JWT_TOKEN);
  } catch (err) {
    throw null;
  }
};

// need for jest mocking
export const tokenFunction = {
  createToken,
  verifToken,
};
