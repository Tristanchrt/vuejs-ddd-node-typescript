import express, { Request, Response } from 'express';
import mongoose from 'mongoose';
import bodyParser from 'body-parser';
import * as dotenv from 'dotenv';
import http from 'http';
import { Routes } from '../routes/routes';

export default class Server {
  readonly app: express.Application;
  readonly server: http.Server;
  readonly port: number;
  readonly routeUser: Routes;

  constructor(port: number) {
    this.port = port;
    this.app = express();
    this.server = http.createServer(this.app);
    this.routeUser = new Routes(this.app);
  }

  // method create of routes in express server
  public routes(): void {
    this.routeUser.configureApiEndPoints();
  }
  // method connexion database
  public connectDB(): void {
    try {
      const url = 'mongodb+srv://root:root@cluster0.sgqw3.gcp.mongodb.net/test';
      mongoose.connect(url, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
      });
      const connection = mongoose.connection;

      connection.once('open', () => {
        console.log(`Connected to Mongoose Server successfully`);
      });
    } catch (err) {
      console.log('Error connect db');
    }
  }

  // method config server
  public config(): void {
    try {
      dotenv.config({ path: __dirname + '/.env' });
      this.app.use(bodyParser.json());
      this.app.use((req: Request, res: Response, next) => {
        res.setHeader('Access-Control-Allow-Origin', '*');
        res.setHeader(
          'Access-Control-Allow-Headers',
          'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization',
        );
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
        next();
      });
    } catch (err) {
      console.log('Erreur start server');
    }
  }
  // method for start server
  public start(): void {
    this.connectDB();
    this.config();
    this.routes();

    this.server.listen(this.port, () => {
      console.log(`Server running on port : ${this.port}`);
    });
  }
  public getApp(): express.Application {
    return this.app;
  }
  public getServer(): http.Server {
    return this.server;
  }
}
