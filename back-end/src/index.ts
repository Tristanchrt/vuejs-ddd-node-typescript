import Server from './server/Server';

try {
  const server: Server = new Server(4000);
  server.start();
} catch (err) {
  throw 'Error start serveur';
}
