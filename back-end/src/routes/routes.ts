import express from 'express';
import { ItemRoute } from './itemRoute/ItemRoute';
import { UserRoute } from './userRoute/UserRoute';

export class Routes {
  private server: express.Application;
  private userRoute: UserRoute;
  private baseUrl = '/api';
  private itemRoute: ItemRoute;

  constructor(server: express.Application) {
    this.server = server;
    this.userRoute = new UserRoute(this.server);
    this.itemRoute = new ItemRoute(this.server);
  }

  public configureApiEndPoints(): void {
    this.userRoute.configureEndPoints(`${this.baseUrl}/user`);
    this.itemRoute.configureEndPoints(`${this.baseUrl}/item`);
  }
}
