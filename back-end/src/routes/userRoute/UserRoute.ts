import express from 'express';
import { UserImplementation } from '../../implementation/user/User';

export class UserRoute extends UserImplementation {
  private server: express.Router;

  constructor(server: express.Router) {
    super();
    this.server = server;
  }
  public configureEndPoints(baseURL: string): void {
    this.server.post(`${baseURL}/create`, this.checkIdentityImplement, this.createImplement);
    this.server.post(`${baseURL}/connect`, this.connectImplement);
    this.server.delete(`${baseURL}/:id`, this.checkIdentityImplement, this.deletOneImplement);
    this.server.get(`${baseURL}/:id`, this.checkIdentityImplement, this.findOneImplement);
    this.server.get(`${baseURL}/`, this.findAllImplement);
  }
}
