import express from 'express';
import { UserImplementation } from '../../implementation/user/User';
import { ItemImplementation } from '../../implementation/item/item';

class UserImplement extends UserImplementation {}

export class ItemRoute extends ItemImplementation {
  private server: express.Router;
  private userImplement: UserImplement;

  constructor(server: express.Router) {
    super();
    this.server = server;
    this.userImplement = new UserImplement();
  }
  public configureEndPoints(baseURL: string): void {
    // this.server.post(`${baseURL}/create`, this.createImplement);
    this.server.delete(`${baseURL}/"id"`, this.deletOneImplement);
    this.server.get(`${baseURL}/:id`, this.findOneImplement);
    this.server.get(`${baseURL}/`, this.findAllImplement);
  }
}
