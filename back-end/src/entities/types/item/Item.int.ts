import { Document } from 'mongoose';
import { IUser } from '../user/User.int';

export interface IItem extends Document {
  title: string;
  price: number;
  priceRange?: Array<any>;
  amount: number;
  shortDescription: string;
  completeDescription: string;
  articlePersonalize: boolean;
  allMark?: number;
  averageMark?: number;
  user?: IUser;
  images?: Array<any>;
  createdAt: Date;

  info(): Partial<IItem>;
}
