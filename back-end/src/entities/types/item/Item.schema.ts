import { model, Schema } from 'mongoose';
import { IItem } from './Item.int';

const ItemSchema = new Schema({
  title: { type: String, required: true },
  price: { type: Number, required: true },
  priceRange: { type: Object, required: false },
  amount: { type: Number, required: true },
  shortDescription: { type: String, required: true },
  completeDescription: { type: String, required: true },
  articlePersonalize: { type: Boolean, required: true },
  allMark: { type: Number, required: false },
  averageMark: { type: Number, required: false },
  user: { type: Schema.Types.ObjectId, ref: 'IUser', required: false },
  images: { type: Array, maxItems: 7 },
  createdAt: { type: Date, default: Date.now },
});

ItemSchema.methods.info = function (): Partial<IItem> {
  const item = {
    _id: this._id,
    title: this.title,
    price: this.price,
    shortDescription: this.shortDescription,
    averageMark: this.averageMark ? this.averageMark : null,
    images: this.images ? this.images : null,
  };
  return item;
};

export const ItemModel = model<IItem>('ItemSchema', ItemSchema);
