import { model, Schema } from 'mongoose';
import { IUser } from './User.int';

const UserSchema = new Schema(
  {
    email: {
      type: String,
      trim: true,
      unique: true,
      lowercase: true,
      required: true,
    },
    password: { type: String, trim: true, required: true },
    firstName: { type: String, trim: true, required: true },
    lastName: { type: String, trim: true, required: true },
    birthDate: { type: Date, required: true },
    avatar: { type: String, trim: true, required: false },
    sex: { type: String, trim: true, required: false },
    nameCompany: { type: String, required: false },
    addressCompany: { type: String, required: false },
    shortDescriptionCompany: { type: String, required: false },
    descriptionCompany: { type: String, required: false },
    postalCodeCompany: { type: Number, required: false },
    createdAt: { type: Date, default: Date.now },
  },
  { timestamps: true },
);

UserSchema.methods.profile = function (): Partial<IUser> {
  const user = {
    _id: this._id,
    avatar: this.avatar ? this.avatar : null,
    firstName: this.firstName,
    lastName: this.lastName,
  };
  return user;
};
UserSchema.methods.company = function (): Partial<IUser> {
  const company = {
    _id: this._id,
    avatar: this.avatar ? this.avatar : null,
    nameCompany: this.nameCompany,
    addressCompany: this.addressCompany,
    shortDescriptionCompany: this.shortDescriptionCompany,
    descriptionCompany: this.descriptionCompany,
    postalCodeCompany: this.postalCodeCompany,
  };
  return company;
};

// // Virtuals
// UserSchema.virtual("fullName").get(function() {
//   return this.firstName + this.lastName
// })

// // Methods
// UserSchema.methods.getGender = function() {
//   return this.gender > 0 "Male" : "Female"
// }

// Static methods
UserSchema.statics.findWithRole = function (id: string): Promise<Partial<IUser>> {
  return this.findById(id).populate('IRole').exec();
};

// // Document middlewares
// UserSchema.pre("save", function(next) {
//   if (this.isModified("password")) {
//     this.password = hashPassword(this.password)
//   }
// });

// // Query middlewares
// UserSchema.post("findOneAndUpdate", async function(doc) {
//   await updateCompanyReference(doc);
// });

export const UserModel = model<IUser>('UserSchema', UserSchema);
