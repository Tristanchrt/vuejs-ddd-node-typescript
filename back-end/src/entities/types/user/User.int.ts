import { Document } from 'mongoose';

export interface IUser extends Document {
  email: string;
  password: string;
  firstName: string;
  lastName: string;
  birthDate: Date;
  avatar?: string;
  sex?: string;
  nameCompany?: string;
  addressCompany?: string;
  shortDescriptionCompany?: string;
  descriptionCompany?: string;
  postalCodeCompany?: number;
  createdAt: Date;

  //methods
  profile(): Partial<IUser>;
  company(): Partial<IUser>;
}
