import { IUser } from '../../../entities/types/user/User.int';

export interface IUserController {
  create(user: any): Promise<Partial<IUser>>;
  findOne(id: string): Promise<Partial<IUser>>;
  checkIdentity(token: string): Promise<void | Error | Partial<IUser>>;
  connect(email: string, password: string): Promise<Partial<IUser>>;
  findAll(): Promise<Array<Partial<IUser>>>;
  deleteOne(id: string): Promise<Partial<any>>;
}
