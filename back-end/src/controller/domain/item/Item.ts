import { IItem } from '../../../entities/types/item/Item.int';

export interface IItemController {
  // create(user: unknown): Promise<Partial<IItem>>;
  findOne(id: string): Promise<Partial<IItem>>;
  findAll(): Promise<Array<Partial<IItem>>>;
  deleteOne(id: string): Promise<Partial<IItem>>;
}
