import { IUserController } from '../../domain/user/User';
import { IUser } from '../../../entities/types/user/User.int';
import { UserModel } from '../../../entities/types/user/User.schema';
import { getTokenId, tokenFunction } from '../../../utils/token';
import { customError, hashVal } from '../../../utils/function';

export abstract class UserController implements IUserController {
  async create(user: any): Promise<Partial<IUser>> {
    try {
      user.password = hashVal(user.password);
      const userSave: IUser = new UserModel({ ...user });
      await userSave.save();
      return userSave.profile();
    } catch (error) {
      throw 'Error during creation user';
    }
  }
  async findOne(id: string): Promise<Partial<IUser>> {
    try {
      const user: IUser = await Promise.resolve(UserModel.findOne({ _id: id }));
      return user.profile();
    } catch (error) {
      throw 'Error for finding user by id';
    }
  }
  async deleteOne(id: string): Promise<Partial<any>> {
    try {
      const deleted: any = await Promise.resolve(UserModel.deleteOne({ _id: id }));
      return deleted;
    } catch (error) {
      throw 'Error to delete user';
    }
  }
  async findAll(): Promise<Array<IUser>> {
    try {
      const users: any = await Promise.resolve(UserModel.find({}));
      return users;
    } catch (error) {
      throw 'Error to find all users';
    }
  }
  async checkIdentity(token: string): Promise<void | Error | Partial<IUser>> {
    try {
      const email: any = getTokenId(token);
      const users: Array<IUser> = await Promise.resolve(UserModel.find());
      let userToReturn: Partial<IUser>;
      users.forEach(function (user) {
        if (hashVal(user.email) == email) {
          userToReturn = user.profile();
        }
      });
      return userToReturn;
    } catch (error) {
      throw 'Error during check identidy';
    }
  }
  async connect(email: string, password: string): Promise<Partial<any>> {
    try {
      const user: IUser = await Promise.resolve(UserModel.findOne({ email: email.toLowerCase() }));
      if (user.password != hashVal(password)) throw 'Error during connection';
      return { ...user.profile(), token: tokenFunction.createToken(user.email) };
    } catch (error) {
      throw 'Error during connection';
    }
  }
}
