import { IItemController } from '../../../controller/domain/item/Item';
import { IItem } from '../../../entities/types/item/Item.int';
import { ItemModel } from '../../../entities/types/item/Item.schema';

export abstract class ItemController implements IItemController {
  async findOne(id: string): Promise<Partial<IItem>> {
    try {
      const item: IItem = await ItemModel.findOne({ _id: id });
      return item.info();
    } catch (error) {
      throw 'Error during creation item';
    }
  }
  async findAll(): Promise<Partial<IItem>[]> {
    try {
      const items: any = await Promise.resolve(ItemModel.find());
      return items;
    } catch (error) {
      throw 'Error to find all items';
    }
  }
  async deleteOne(id: string): Promise<Partial<IItem>> {
    try {
      const itemDeleted: any = await Promise.resolve(ItemModel.deleteOne({ _id: id }));
      return itemDeleted;
    } catch (error) {
      throw 'Error to delete item';
    }
  }

  // async create(item: any): Promise<Partial<IItem>> {
  //   try {
  //     const itemCreated: any = new ItemModel({ ...item });
  //     await itemCreated.save();
  //     return itemCreated;
  //   } catch (error) {
  //     throw 'Error to created item' + error;
  //   }
  // }
}
