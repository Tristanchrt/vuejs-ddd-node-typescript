import { Request, Response, NextFunction } from 'express';
import { UserController } from '../../controller/use-cases/user/User';
import { tokenFunction } from '../../utils/token';

export abstract class UserImplementation extends UserController {
  public checkIdentityImplement = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      if (!tokenFunction.verifToken(req.headers.authorization)) {
        res.status(404);
        res.json('request prohibited');
      }
      await this.checkIdentity(req.headers.authorization)
        .then((user: any) => {
          next();
        })
        .catch((error: any) => {
          res.status(404);
          res.json(error);
        });
    } catch (error) {
      res.status(404);
      res.json('request prohibited');
    }
  };

  public connectImplement = async (req: Request, res: Response): Promise<void> => {
    await this.connect(req.body.email, req.body.password)
      .then((user: any) => {
        res.status(200);
        res.json(user);
      })
      .catch((error: any) => {
        res.status(404);
        res.json(error);
      });
  };

  public findAllImplement = async (req: Request, res: Response): Promise<void> => {
    await this.findAll()
      .then((users: any) => {
        res.status(200);
        res.json(users);
      })
      .catch((error: any) => {
        res.status(404);
        res.json(error);
      });
  };
  public deletOneImplement = async (req: Request, res: Response): Promise<void> => {
    await this.deleteOne(req.params.id)
      .then((userDeleted: any) => {
        res.status(200);
        res.json(userDeleted);
      })
      .catch((error: any) => {
        res.status(404);
        res.json(error);
      });
  };

  public createImplement = async (req: Request, res: Response): Promise<void> => {
    await this.create(req.body)
      .then((user: any) => {
        res.status(201);
        res.json(user);
      })
      .catch((error: any) => {
        res.status(404);
        res.json(error);
      });
  };

  public findOneImplement = async (req: Request, res: Response): Promise<void> => {
    try {
      await this.findOne(req.params.id)
        .then((user: any) => {
          res.status(200);
          res.json(user);
        })
        .catch((error: any) => {
          res.status(404);
          res.json(error);
        });
    } catch (error) {
      res.status(404);
      res.json('Error for find user');
    }
  };
}
