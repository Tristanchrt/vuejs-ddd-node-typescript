import { Request, Response } from 'express';
import { ItemController } from '../../controller/use-cases/item/Item';

export abstract class ItemImplementation extends ItemController {
  // public createImplement = async (req: Request, res: Response): Promise<void> => {
  //   await this.create(req.body)
  //     .then((item: any) => {
  //       res.status(201);
  //       res.json(item.info());
  //     })
  //     .catch((error: any) => {
  //       res.status(404).json(error);
  //     });
  // };

  public findOneImplement = async (req: Request, res: Response): Promise<void> => {
    try {
      await this.findOne(req.params.id)
        .then((item: any) => {
          res.status(200);
          res.json(item);
        })
        .catch((error: any) => {
          res.status(404);
          res.json(error);
        });
    } catch (error) {
      res.status(404);
      res.json('Error for find');
    }
  };

  public deletOneImplement = async (req: Request, res: Response): Promise<void> => {
    await this.deleteOne(req.params.id)
      .then((items: any) => {
        res.status(200);
        res.json(items);
      })
      .catch((error: any) => {
        res.status(404);
        res.json(error);
      });
  };

  public findAllImplement = async (req: Request, res: Response): Promise<void> => {
    await this.findAll()
      .then((items: any) => {
        res.status(200);
        res.json(items);
      })
      .catch((error: any) => {
        res.status(404);
        res.json(error);
      });
  };
}
