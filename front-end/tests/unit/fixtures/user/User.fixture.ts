import { User } from '@/domain/user/User';
import { RestUser } from '@/secondary/restUser/RestUser';

const restUser1: RestUser = {
  _id: '5fd3e54d7d88981a74149f24',
  email: 'moto@yahoo.fr',
  password: 'moto',
  firstName: 'moto',
  lastName: 'moto',
  birthDate: new Date('11/11/1999'),
  createdAt: new Date('11/11/1999'),
};
const restUser2: RestUser = {
  _id: '5fd3e54d7d88981a74149f25',
  email: 'titi@yahoo.fr',
  password: 'titi',
  firstName: 'titi',
  lastName: 'titi',
  birthDate: new Date('11/11/1999'),
  createdAt: new Date('11/11/1999'),
  avatar: 'someAvatar.jpg',
};
const user1: User = {
  id: '5fd3e54d7d88981a74149f24',
  email: 'moto@yahoo.fr',
  name: 'moto',
  picture: null,
};
const user2: User = {
  id: '5fd3e54d7d88981a74149f25',
  email: 'titi@yahoo.fr',
  name: 'titi',
  picture: 'someAvatar.jpg',
};

export { restUser1, restUser2, user1, user2 };
