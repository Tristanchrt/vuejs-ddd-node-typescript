import { ModalStoreModuleStub, resolveModalStoreModule, stubModalStoreModule } from './ModalStoreModule.fixture';
import { resolveUserStoreModule, stubUserStoreModule, UserStoreModuleStub } from './UserStoreModule.fixture';

import { StoreRepository } from '@/domain/store/StoreRepository';

export class StubStoreRepository implements StoreRepository {
  constructor(private userStore: UserStoreModuleStub, private modalStore: ModalStoreModuleStub) {}

  user() {
    return this.userStore;
  }
  modal() {
    return this.modalStore;
  }
}

export const stubStoreRepository = (): StubStoreRepository => {
  const storeRepository = new StubStoreRepository(stubUserStoreModule(), stubModalStoreModule());
  return storeRepository;
};

export const resolveStoreRepository = (): StubStoreRepository => {
  const storeRepository = new StubStoreRepository(resolveUserStoreModule(), resolveModalStoreModule());
  return storeRepository;
};
