import sinon, { SinonStub } from 'sinon';

import { ModalStoreDomain } from '@/domain/store/StoreRepository';

export interface ModalStoreModuleStub extends ModalStoreDomain {
  resetState: SinonStub;
  getModal: SinonStub;
  open: SinonStub;
  close: SinonStub;
}
export const stubModalStoreModule = (): ModalStoreModuleStub => ({
  resetState: sinon.stub(),
  getModal: sinon.stub(),
  open: sinon.stub(),
  close: sinon.stub(),
});
export const resolveModalStoreModule = (): ModalStoreModuleStub => {
  const userStoreModule = stubModalStoreModule();
  userStoreModule.resetState.returns(null);
  userStoreModule.getModal.returns(null);
  userStoreModule.open.returns(undefined);
  userStoreModule.close.returns(null);
  return userStoreModule;
};
