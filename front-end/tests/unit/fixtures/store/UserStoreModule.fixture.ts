import sinon, { SinonStub } from 'sinon';

import { UserStoreDomain } from '@/domain/store/StoreRepository';

export interface UserStoreModuleStub extends UserStoreDomain {
  resetState: SinonStub;
  getUser: SinonStub;
  setUser: SinonStub;
  getToken: SinonStub;
  setToken: SinonStub;
}
export const stubUserStoreModule = (): UserStoreModuleStub => ({
  resetState: sinon.stub(),
  getUser: sinon.stub(),
  setUser: sinon.stub(),
  getToken: sinon.stub(),
  setToken: sinon.stub(),
});
export const resolveUserStoreModule = (): UserStoreModuleStub => {
  const userStoreModule = stubUserStoreModule();
  userStoreModule.getUser.returns('someValue');
  userStoreModule.setUser.returns('someValue');
  userStoreModule.getToken.returns('someToken');
  userStoreModule.setToken.returns('someToken');
  return userStoreModule;
};
