import sinon, { SinonStub } from 'sinon';

import { item1, item2 } from './Item.fixture';

import { ItemRepository } from '@/domain/item/ItemRepository';

interface RestItemRepository extends ItemRepository {
  getItems: SinonStub;
  getItem: SinonStub;
  updateItem: SinonStub;
}

export const stubItemRepository = (): RestItemRepository => ({
  getItems: sinon.stub(),
  getItem: sinon.stub(),
  updateItem: sinon.stub(),
});

export const resolveItemRepository = (): RestItemRepository => {
  const itemRepository = stubItemRepository();
  itemRepository.getItems.resolves([item1, item2]);
  itemRepository.getItem.resolves(item1);
  itemRepository.updateItem.resolves(item1);
  return itemRepository;
};
