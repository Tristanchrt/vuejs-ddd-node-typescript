import { restUser1, user1 } from '../user/User.fixture';

import { Item } from '@/domain/item/Item';
import { RestItem } from '@/secondary/restItem/RestItem';

const restItem1: RestItem = {
  _id: '5fd3e54d7d88981a74149f26',
  title: 'Porte en bois',
  price: 150,
  amount: 1000,
  shortDescription: 'some short description',
  articlePersonalize: false,
  allMark: 15,
  averageMark: 4.5,
  user: restUser1,
  createdAt: new Date('11/11/2018'),
};
const item1: Item = {
  id: '5fd3e54d7d88981a74149f26',
  title: 'Porte en bois',
  price: 150,
  shortDescription: 'some short description',
  allMark: 15,
  averageMark: 4.5,
  user: user1,
  images: null,
};
const restItem2: RestItem = {
  _id: '5fd3e54d7d88981a74149f27',
  title: 'T-shirt bleu',
  price: 25,
  amount: 15000,
  shortDescription: 'some short description for t-shirt',
  articlePersonalize: true,
  allMark: 15,
  averageMark: 4.5,
  user: null,
  createdAt: new Date('11/11/2015'),
  images: ['image1.jpg', 'image2.jpg'],
};
const item2: Item = {
  id: '5fd3e54d7d88981a74149f27',
  title: 'T-shirt bleu',
  price: 25,
  shortDescription: 'some short description for t-shirt',
  allMark: 15,
  averageMark: 4.5,
  user: null,
  images: ['image1.jpg', 'image2.jpg'],
};

export { restItem1, restItem2, item1, item2 };
