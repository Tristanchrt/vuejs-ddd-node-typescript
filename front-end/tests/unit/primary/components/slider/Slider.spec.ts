import { shallowMount, Wrapper } from '@vue/test-utils';
import VueRouter from 'vue-router';

import { stubRouter } from '../../..//Utils';

import { SliderComponent, SliderVue } from '@/primary/components/slider';

let wrapper: Wrapper<SliderComponent>;
let component: SliderComponent;
const $router: VueRouter = stubRouter();

const wrap = () => {
  wrapper = shallowMount<SliderComponent>(SliderVue, {
    mocks: {
      $router,
    },
  });
  component = wrapper.vm;
};

describe('SliderComponent', () => {
  it('Should exists', () => {
    wrap();
    expect(wrapper.exists()).toBeTruthy();
  });
  it('Should add 1 to currentIndex propriety', () => {
    wrap();
    expect(component.currentIndex).toEqual(2);
    component.next();
    expect(component.currentIndex).toEqual(3);
    component.next();
    component.next();
    expect(component.currentIndex).toEqual(4);
  });
  it('Should remove add 1 to currentIndex propriety', () => {
    wrap();
    expect(component.currentIndex).toEqual(2);
    component.prev();
    expect(component.currentIndex).toEqual(1);
    component.prev();
    component.prev();
    expect(component.currentIndex).toEqual(0);
  });
});
