import { shallowMount, Wrapper } from '@vue/test-utils';
import VueRouter from 'vue-router';

import { stubRouter } from '../../../Utils';

import { HeaderComponent, HeaderVue } from '@/primary/components/header';

let wrapper: Wrapper<HeaderComponent>;
let component: HeaderComponent;
const $router: VueRouter = stubRouter();

const wrap = () => {
  wrapper = shallowMount<HeaderComponent>(HeaderVue, {
    mocks: {
      $router,
    },
  });
  component = wrapper.vm;
};

describe('HeaderComponent', () => {
  it('Should exists', () => {
    wrap();
    expect(wrapper.exists()).toBeTruthy();
  });
});
