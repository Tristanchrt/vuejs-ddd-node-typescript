import { shallowMount, Wrapper } from '@vue/test-utils';
import VueRouter from 'vue-router';

import { stubRouter } from '../../../Utils';

import { InputHeaderComponent, InputHeaderVue } from '@/primary/components/header/inputHeader';

let wrapper: Wrapper<InputHeaderComponent>;
let component: InputHeaderComponent;
const $router: VueRouter = stubRouter();

const wrap = () => {
  wrapper = shallowMount<InputHeaderComponent>(InputHeaderVue, {
    mocks: {
      $router,
    },
  });
  component = wrapper.vm;
};

describe('InputHeaderComponent', () => {
  it('Should exists', () => {
    wrap();
    expect(wrapper.exists()).toBeTruthy();
  });
});
