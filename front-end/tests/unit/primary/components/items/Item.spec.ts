import { shallowMount, Wrapper } from '@vue/test-utils';
import VueRouter from 'vue-router';

import { item1 } from '../../../fixtures/item/Item.fixture';
import { stubStoreRepository } from '../../../fixtures/store/StoreRepository.fixture';
import { stubRouter } from '../../../Utils';

import { StoreRepository } from '@/domain/store/StoreRepository';
import { ItemComponent, ItemVue } from '@/primary/components/items/item';
import RestStoreRepository from '@/secondary/store/RestStoreRepository';

let wrapper: Wrapper<ItemComponent>;
let component: ItemComponent;
const $router: VueRouter = stubRouter();
let storeRepository = new RestStoreRepository();

interface WapParams {
  storeRepository?: StoreRepository;
}

const wrap = (params?: WapParams) => {
  const storeRepository = params?.storeRepository ? params?.storeRepository : stubStoreRepository();
  wrapper = shallowMount<ItemComponent>(ItemVue, {
    mocks: {
      $router,
    },
    provide: {
      storeRepository: () => storeRepository,
    },
    propsData: {
      item: item1,
    },
  });
  component = wrapper.vm;
};

describe('ItemComponent', () => {
  it('Should exists', () => {
    wrap();
    expect(wrapper.exists()).toBeTruthy();
  });
  it('Should open modal', () => {
    wrap();
    const modalOpened = jest.spyOn(component, 'openModal');

    component.openModal();
    expect(modalOpened).toBeCalled();
    expect(storeRepository.modal().getModal()).toEqual(null);
    expect(component.modal).toEqual(null);
  });
});
