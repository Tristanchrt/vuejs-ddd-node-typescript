import { shallowMount, Wrapper } from '@vue/test-utils';
import VueRouter from 'vue-router';

import { resolveItemRepository } from '../../../fixtures/item/ItemRepository.fixture';
import { resolveStoreRepository } from '../../../fixtures/store/StoreRepository.fixture';
import { stubRouter } from '../../../Utils';

import { ItemRepository } from '@/domain/item/ItemRepository';
import { StoreRepository } from '@/domain/store/StoreRepository';
import { ItemsComponent, ItemsVue } from '@/primary/components/items';
import RestStoreRepository from '@/secondary/store/RestStoreRepository';

let wrapper: Wrapper<ItemsComponent>;
let component: ItemsComponent;
const $router: VueRouter = stubRouter();
let storeRepository = new RestStoreRepository();

interface WapParams {
  storeRepository?: StoreRepository;
  itemRepository?: ItemRepository;
}

const wrap = (params?: WapParams) => {
  const storeRepository = params?.storeRepository ? params?.storeRepository : resolveStoreRepository();
  const itemRepository = params?.itemRepository ? params?.itemRepository : resolveItemRepository();
  wrapper = shallowMount<ItemsComponent>(ItemsVue, {
    mocks: {
      $router,
    },
    provide: {
      storeRepository: () => storeRepository,
      itemRepository: () => itemRepository,
    },
  });
  component = wrapper.vm;
};

describe('ItemsComponent', () => {
  it('Should exists', () => {
    wrap();
    expect(wrapper.exists()).toBeTruthy();
  });
  it('Should select chat and put it in token', () => {
    wrap({ storeRepository: storeRepository });
    const itemIdSelected = jest.spyOn(component, 'selectChat');
    component.selectChat('someValue');
    expect(itemIdSelected).toBeCalled();
    expect(storeRepository.user().getToken()).toEqual('someValue');
  });
});
