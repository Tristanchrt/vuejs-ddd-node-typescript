import { shallowMount, Wrapper } from '@vue/test-utils';
import VueRouter from 'vue-router';

import { item1 } from '../../../fixtures/item/Item.fixture';
import { stubStoreRepository } from '../../../fixtures/store/StoreRepository.fixture';
import { stubRouter } from '../../../Utils';

import { StoreRepository } from '@/domain/store/StoreRepository';
import { ModalComponent, ModalVue } from '@/primary/components/modal';
import RestStoreRepository from '@/secondary/store/RestStoreRepository';

let wrapper: Wrapper<ModalComponent>;
let component: ModalComponent;
const $router: VueRouter = stubRouter();
let storeRepository = new RestStoreRepository();

interface WapParams {
  storeRepository?: StoreRepository;
}

const wrap = (params?: WapParams) => {
  const storeRepository = params?.storeRepository ? params?.storeRepository : stubStoreRepository();
  wrapper = shallowMount<ModalComponent>(ModalVue, {
    mocks: {
      $router,
    },
    provide: {
      storeRepository: () => storeRepository,
    },
    propsData: {
      item: item1,
    },
  });
  component = wrapper.vm;
};

describe('ModalComponent', () => {
  it('Should exists', () => {
    wrap();
    expect(wrapper.exists()).toBeTruthy();
  });
  it('Should close modal', () => {
    wrap();
    const modalClosed = jest.spyOn(component, 'close');

    component.close();
    expect(modalClosed).toBeCalled();
    expect(storeRepository.modal().getModal()).toEqual(null);
  });
});
