import { shallowMount, Wrapper } from '@vue/test-utils';
import VueRouter from 'vue-router';

import { stubRouter } from '../Utils';

import { AppComponent, AppVue } from '@/primary/app';

let wrapper: Wrapper<AppComponent>;
let component: AppComponent;
const $router: VueRouter = stubRouter();

const wrap = () => {
  wrapper = shallowMount<AppComponent>(AppVue, {
    mocks: {
      $router,
    },
    stubs: ['router-link', 'router-view'],
  });
  component = wrapper.vm;
};

describe('App', () => {
  it('Should exists', () => {
    wrap();
    expect(wrapper.exists()).toBeTruthy();
  });
});
