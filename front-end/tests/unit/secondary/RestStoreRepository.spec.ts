import { user1 } from '../fixtures/user/User.fixture';

import { User } from '@/domain/user/User';
import { LocalStorage } from '@/secondary/store/LocalStorage';
import { ModalStoreClass } from '@/secondary/store/ModalStore';
import RestStoreRepository from '@/secondary/store/RestStoreRepository';
import { UserStoreClass } from '@/secondary/store/UserStore';

let storeRepository = new RestStoreRepository();

describe('RestStoreRepository', () => {
  beforeEach(() => {
    localStorage.clear();
    storeRepository.user().resetState();
    storeRepository.modal().resetState();
  });
  it('Should create stote', () => {
    let _storeRepository = new RestStoreRepository(new UserStoreClass(), new ModalStoreClass());
  });
  it('Should correctly set & get user/token', () => {
    const setUserTokenToLocalStorage = jest.spyOn(LocalStorage, 'setUserToken');
    const token: string = storeRepository.user().setToken('someToken');

    expect(setUserTokenToLocalStorage).toBeCalledWith('someToken');
    expect(storeRepository.user().getToken()).toBe('someToken');
    expect(token).toBe('someToken');
  });
  it('Should correctly set & get user', () => {
    const user: User = storeRepository.user().setUser(user1);

    expect(storeRepository.user().getUser()).toEqual(user1);
    expect(user).toEqual(user1);
  });
  it('Should correctly open & close & get', () => {
    storeRepository.modal().open(undefined);
    expect(storeRepository.modal().getModal()).toEqual(undefined);
    storeRepository.modal().close();
    expect(storeRepository.modal().getModal()).toEqual(null);
  });
});
