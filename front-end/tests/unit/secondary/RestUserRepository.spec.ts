import { resolveStoreRepository } from '../fixtures/store/StoreRepository.fixture';
import { restUser2, user2 } from '../fixtures/user/User.fixture';
import { customError, stubAxiosInstance } from '../Utils';

import { User } from '@/domain/user/User';
import RestUserRepository from '@/secondary/restUser/RestUserRepository';

describe('RestUserRepository', () => {
  it('Should fail to signIn', async () => {
    const axiosInstance = stubAxiosInstance();
    const storeRepository = resolveStoreRepository();
    axiosInstance.post.rejects(customError(500, "Can't sign in: Network Error"));

    const restUserRepository = new RestUserRepository(storeRepository, axiosInstance);
    await restUserRepository.signIn('someEmail', 'somePassword').catch(error => {
      expect(error).toEqual(Error("Can't sign in: Network Error"));
    });
  });
  it('Should correctly sign in with the right parameters', async () => {
    const axiosInstance = stubAxiosInstance();
    const storeRepository = resolveStoreRepository();
    axiosInstance.post.resolves({ data: restUser2 });
    const axiosPost = jest.spyOn(axiosInstance, 'post');

    const restUserRepository = new RestUserRepository(storeRepository, axiosInstance);
    const setUserTokenToStore = jest.spyOn(storeRepository.user(), 'setUser');
    const user: User = await restUserRepository.signIn('someEmail', 'somePassword');
    expect(axiosPost).toBeCalledWith('/user/connect', {
      email: 'someEmail',
      password: 'somePassword',
    });
    expect(user).toEqual(user2);
    expect(setUserTokenToStore).toBeCalledWith(user2);
  });
});
