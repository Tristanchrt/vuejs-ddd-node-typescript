import { item1, item2, restItem1, restItem2 } from '../fixtures/item/Item.fixture';
import { customError, stubAxiosInstance } from '../Utils';

import { Item } from '@/domain/item/Item';
import RestItemRepository from '@/secondary/restItem/RestItemRepository';

describe('RestItemRepository', () => {
  it('Should fail to get items', async () => {
    const axiosInstance = stubAxiosInstance();
    axiosInstance.get.rejects(customError(500, "Can't get items: Network Error"));
    const restItemRepository = new RestItemRepository(axiosInstance);
    await restItemRepository.getItems().catch(error => {
      expect(error).toEqual(Error("Can't get items: Network Error"));
    });
  });
  it('Should correctly get all items', async () => {
    const axiosInstance = stubAxiosInstance();
    axiosInstance.get.resolves({ data: [restItem1, restItem2] });
    const restItemRepository = new RestItemRepository(axiosInstance);
    const items: Array<Item> = await restItemRepository.getItems();
    expect(JSON.stringify(items)).toEqual(JSON.stringify([item1, item2]));
  });
  it('Should fail to get item', async () => {
    const axiosInstance = stubAxiosInstance();
    axiosInstance.get.rejects(customError(500, "Can't get item: Network Error"));
    const restItemRepository = new RestItemRepository(axiosInstance);
    await restItemRepository.getItem('someId').catch(error => {
      expect(error).toEqual(Error("Can't get item: Network Error"));
    });
  });
  it('Should correctly get item with the right parameters', async () => {
    const axiosInstance = stubAxiosInstance();
    axiosInstance.get.resolves({ data: restItem2 });
    const axiosGet = jest.spyOn(axiosInstance, 'get');
    const restItemRepository = new RestItemRepository(axiosInstance);
    const item: Item = await restItemRepository.getItem(restItem2._id);
    expect(axiosGet).toBeCalledWith('/item/' + restItem2._id);
    expect(item).toEqual(item2);
  });
  it('Should fail to updated item', async () => {
    const axiosInstance = stubAxiosInstance();
    axiosInstance.put.rejects(customError(500, "Can't update item: Network Error"));
    const restItemRepository = new RestItemRepository(axiosInstance);
    await restItemRepository.updateItem(item1).catch(error => {
      expect(error).toEqual(Error("Can't update item: Network Error"));
    });
  });

  it('Should correctly updated item with the right parameters', async () => {
    const axiosInstance = stubAxiosInstance();
    axiosInstance.put.resolves({ data: restItem1 });
    const axiosPut = jest.spyOn(axiosInstance, 'put');
    const restItemRepository = new RestItemRepository(axiosInstance);
    const item: Item = await restItemRepository.updateItem(item1);
    expect(axiosPut).toBeCalledWith('/item/', {
      item: item1,
    });
    expect(JSON.stringify(item)).toEqual(JSON.stringify(item1));
  });
});
