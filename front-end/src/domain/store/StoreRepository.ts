import { User } from '../user/User';

import { ModalOptions } from '@/secondary/store/ModalStore';

export interface UserStoreDomain {
  resetState(): void;
  getUser(): User;
  setUser(value: User): User;
  getToken(): string;
  setToken(value: string): string;
}
export interface ModalStoreDomain {
  resetState(): void;
  getModal(): void;
  open(options?: ModalOptions): void;
  close(): void;
}

export interface StoreRepository {
  user(): UserStoreDomain;
  modal(): ModalStoreDomain;
}
