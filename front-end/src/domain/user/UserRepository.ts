import { User } from './User';

export interface UserRepository {
  signIn(email: string, password: string): Promise<User>;
}
