export interface User {
  id: string;
  name: string;
  email: string;
  picture?: string | null;
}

export enum Sex {
  Male = 'Male',
  Female = 'Female',
}
