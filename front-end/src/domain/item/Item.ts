import { User } from '../user/User';

export interface Item {
  id: string;
  title: string;
  price: number;
  shortDescription: string;
  allMark: number;
  averageMark: number;
  user?: User | null;
  images?: Array<any> | null;
}
