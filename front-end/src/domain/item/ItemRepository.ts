import { Item } from './Item';

export interface ItemRepository {
  getItems(): Promise<Array<Item>>;
  getItem(idItem: string): Promise<Item>;
  updateItem(item: Item): Promise<Item>;
}
