import { RestUser, toUser } from '../restUser/RestUser';

import { Item } from '@/domain/item/Item';

export interface RestItem {
  _id: string;
  title: string;
  price: number;
  priceRange?: Array<any>;
  amount: number;
  shortDescription: string;
  completeDescription?: string;
  articlePersonalize: boolean;
  allMark: number;
  averageMark: number;
  user?: RestUser | null;
  images?: Array<any>;
  createdAt: Date;
}

export const toItem = (restItem: RestItem): Item => ({
  id: restItem._id,
  title: restItem.title,
  price: restItem.price,
  shortDescription: restItem.shortDescription,
  allMark: restItem.allMark,
  averageMark: restItem.averageMark,
  user: restItem.user ? toUser(restItem.user) : null,
  images: restItem.images ? restItem.images : null,
});
