import { AxiosInstance, AxiosResponse } from 'axios';

import { RestItem, toItem } from './RestItem';

import { Item } from '@/domain/item/Item';
import { ItemRepository } from '@/domain/item/ItemRepository';

export default class RestItemRepository implements ItemRepository {
  private axiosInstance: AxiosInstance;
  private baseURL: string;

  constructor(axiosInstance: AxiosInstance) {
    this.axiosInstance = axiosInstance;
    this.baseURL = '/item';
  }

  async getItems(): Promise<Item[]> {
    return this.axiosInstance
      .get(this.baseURL + '/')
      .then((response: AxiosResponse<RestItem[]>) => response.data.map(resp => toItem(resp)))
      .catch(error => {
        throw new Error(error.message);
      });
  }
  async getItem(idItem: string): Promise<Item> {
    return this.axiosInstance
      .get(this.baseURL + '/' + idItem)
      .then((response: AxiosResponse<RestItem>) => {
        const user = toItem(response.data);
        return user;
      })
      .catch(error => {
        throw new Error(error.message);
      });
  }
  async updateItem(itemToUpdate: Item): Promise<Item> {
    return this.axiosInstance
      .put(this.baseURL + '/', {
        item: itemToUpdate,
      })
      .then((response: AxiosResponse<RestItem>) => {
        const user = toItem(response.data);
        return user;
      })
      .catch(error => {
        throw new Error(error.message);
      });
  }
}
