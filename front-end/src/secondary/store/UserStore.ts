import { Module } from 'vuex';

import { UserStoreDomain } from '../../domain/store/StoreRepository';
import { User } from '../../domain/user/User';

import { LocalStorage } from './LocalStorage';

const defaultState = () => ({
  token: LocalStorage.getUserToken(),
  user: null,
});

const userStoreModule: Module<any, any> = {
  namespaced: true,
  state: defaultState(),
};

class UserStoreClass implements UserStoreDomain {
  private module: Module<any, any> = userStoreModule;
  private state = this.module.state;

  public resetState() {
    LocalStorage.removeUserToken();
    this.state = defaultState();
  }

  public getUser(): User {
    return this.state.user;
  }
  public setUser(value: User): User {
    this.state.user = value;
    return this.state.user;
  }

  public getToken(): string {
    return this.state.token;
  }
  public setToken(value: string): string {
    LocalStorage.setUserToken(value);
    this.state.token = value;
    return this.state.token;
  }
}

export { userStoreModule, UserStoreClass };
