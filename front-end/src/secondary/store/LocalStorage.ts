export class LocalStorage {
  static userToken = 'token';

  static setUserToken(token: any) {
    localStorage.setItem(this.userToken, token!);
  }

  static removeUserToken(): boolean {
    localStorage.removeItem(this.userToken);
    return true;
  }

  static getUserToken(): string | null {
    return localStorage.getItem(this.userToken);
  }
}
