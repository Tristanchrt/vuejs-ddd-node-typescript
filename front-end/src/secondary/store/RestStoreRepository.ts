import Vue from 'vue';
import Vuex, { Store } from 'vuex';

// import { SocketStoreClass, socketStoreModule } from './SocketStore';
import { ModalStoreClass, modalStoreModule } from './ModalStore';
import { userStoreModule, UserStoreClass } from './UserStore';

import { ModalStoreDomain, StoreRepository, UserStoreDomain } from '@/domain/store/StoreRepository';

Vue.use(Vuex);

export const store = new Store({
  modules: {
    user: userStoreModule,
    modal: modalStoreModule,
  },
});

export default class RestStoreRepository implements StoreRepository {
  private store: Store<any> = store;
  private userStoreClass: UserStoreDomain;
  private modalStoreClass: ModalStoreDomain;

  constructor(userStoreClass?: UserStoreDomain, modalStoreClass?: ModalStoreDomain) {
    this.userStoreClass = userStoreClass ? userStoreClass : new UserStoreClass();
    this.modalStoreClass = modalStoreClass ? modalStoreClass : new ModalStoreClass();
  }

  public user(): UserStoreDomain {
    return this.userStoreClass;
  }
  public modal(): ModalStoreDomain {
    return this.modalStoreClass;
  }
}
