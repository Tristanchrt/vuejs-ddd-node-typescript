import { VueConstructor } from 'vue';
import { Module } from 'vuex';

import { ModalStoreDomain } from '@/domain/store/StoreRepository';

export interface ModalOptions {
  component: VueConstructor;
  props?: Object;
}

const defaultState = () => ({
  modalValue: null,
});

const modalStoreModule: Module<any, any> = {
  namespaced: true,
  state: defaultState(),
};

class ModalStoreClass implements ModalStoreDomain {
  private module: Module<any, any> = modalStoreModule;
  private state = this.module.state;

  public resetState() {
    this.state = defaultState();
  }

  getModal(): any | null {
    return this.state.modalValue;
  }

  open(options?: ModalOptions): void {
    this.state.modalValue = options;
  }
  close(): void {
    this.state.modalValue = null;
  }
}

export { modalStoreModule, ModalStoreClass };
