import { User } from '@/domain/user/User';

export interface RestUser {
  _id: string;
  email: string;
  password: string;
  firstName: string;
  lastName: string;
  birthDate: Date;
  avatar?: string;
  sex?: string;
  nameCompany?: string;
  addressCompany?: string;
  shortDescriptionCompany?: string;
  descriptionCompany?: string;
  postalCodeCompany?: number;
  createdAt: Date;
}

export const toUser = (restUser: RestUser): User => ({
  id: restUser._id,
  email: restUser.email,
  name: restUser.firstName,
  picture: restUser.avatar ? restUser.avatar : null,
});
