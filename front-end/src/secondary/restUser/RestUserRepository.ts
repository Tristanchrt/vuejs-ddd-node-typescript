import { AxiosInstance, AxiosResponse } from 'axios';

import { StoreRepository } from '../../domain/store/StoreRepository';
import { User } from '../../domain/user/User';
import { UserRepository } from '../../domain/user/UserRepository';

import { RestUser, toUser } from './RestUser';

export default class RestUserRepository implements UserRepository {
  private axiosInstance: AxiosInstance;
  private baseURL: string;
  private storeRepository: StoreRepository;
  constructor(storeRepository: StoreRepository, axiosInstance: AxiosInstance) {
    this.storeRepository = storeRepository;
    this.axiosInstance = axiosInstance;
    this.baseURL = '/user';
  }

  async signIn(email: string, password: string): Promise<User> {
    return this.axiosInstance
      .post(this.baseURL + '/connect', {
        email: email,
        password: password,
      })
      .then((response: AxiosResponse<RestUser>) => {
        const user = toUser(response.data);
        this.storeRepository.user().setUser(user);
        return user;
      })
      .catch(error => {
        throw new Error(error.message);
      });
  }
}
