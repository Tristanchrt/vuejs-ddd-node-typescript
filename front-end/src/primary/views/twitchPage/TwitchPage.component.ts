import { Component, Inject, Prop, Vue, Watch } from 'vue-property-decorator';

import { ItemRepository } from '@/domain/item/ItemRepository';
import { UserRepository } from '@/domain/user/UserRepository';
import { ItemsVue } from '@/primary/components/items';
import { SliderVue } from '@/primary/components/slider/';
import { HeaderVue } from '@/primary/components/twitch/header/index';

@Component({
  components: {
    HeaderVue,
  },
})
export default class TwitchPageComponent extends Vue {}
