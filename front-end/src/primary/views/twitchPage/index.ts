import TwitchPageComponent from '@/primary/views/twitchPage/TwitchPage.component';
import TwitchPageVue from '@/primary/views/twitchPage/TwitchPage.vue';

export { TwitchPageComponent, TwitchPageVue };
