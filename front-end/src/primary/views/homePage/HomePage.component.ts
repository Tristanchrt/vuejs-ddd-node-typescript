import { Component, Inject, Prop, Vue, Watch } from 'vue-property-decorator';

import { ItemRepository } from '@/domain/item/ItemRepository';
import { UserRepository } from '@/domain/user/UserRepository';
import { ItemsVue } from '@/primary/components/items';
import { SliderVue } from '@/primary/components/slider/';

@Component({
  components: {
    ItemsVue,
    SliderVue,
  },
})
export default class HomePageComponent extends Vue {}
