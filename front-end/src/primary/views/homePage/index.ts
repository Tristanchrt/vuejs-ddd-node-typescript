import HomePageComponent from '@/primary/views/homePage/HomePage.component';
import HomePageVue from '@/primary/views/homePage/HomePage.vue';

export { HomePageComponent, HomePageVue };
