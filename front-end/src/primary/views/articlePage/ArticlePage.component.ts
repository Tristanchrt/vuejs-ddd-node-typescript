import { Component, Inject, Prop, Vue, Watch } from 'vue-property-decorator';

import { HeaderArticleVue } from '@/primary/components/headerArticle/';

@Component({
  components: {
    HeaderArticleVue,
  },
})
export default class ArticlePageComponent extends Vue {
  mainImage: string = 'https://miro.medium.com/max/8050/1*BU8NA9H5RwqrneVrQBlBlw.jpeg';
  descriptionImage: string = 'https://image.isu.pub/200506222003-b39ccf222d559ae4cb481f8e33835aad/jpg/page_1.jpg';
  faces: Array<string> = [
    'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR6RzTZv3NNtD9PvGtd9WxT4HDRF47DKFIybw&usqp=CAU',
    'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR6RzTZv3NNtD9PvGtd9WxT4HDRF47DKFIybw&usqp=CAU',
    'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR6RzTZv3NNtD9PvGtd9WxT4HDRF47DKFIybw&usqp=CAU',
    'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR6RzTZv3NNtD9PvGtd9WxT4HDRF47DKFIybw&usqp=CAU',
    'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR6RzTZv3NNtD9PvGtd9WxT4HDRF47DKFIybw&usqp=CAU',
  ];
  advantages: Array<string> = ['Routine', 'dffdf ,mldfm', 'Afdg lkpgfdp ;pgp;hg;p', 'wqs;mq gfgfaaa ;pgp;hg;p', 'Afdg lkpgfdp '];
}
