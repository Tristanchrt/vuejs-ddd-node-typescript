import ArticlePageComponent from '@/primary/views/articlePage/ArticlePage.component';
import ArticlePageVue from '@/primary/views/articlePage/ArticlePage.vue';

export { ArticlePageComponent, ArticlePageVue };
