import { Component, Inject, Prop, Vue, Watch } from 'vue-property-decorator';

import { Item } from '@/domain/item/Item';
import { StoreRepository } from '@/domain/store/StoreRepository';

@Component({})
export default class ModalComponent extends Vue {
  @Prop({ type: Object })
  item!: Item;

  @Inject()
  private storeRepository!: () => StoreRepository;

  close = () => {
    this.storeRepository()
      .modal()
      .close();
  };
}
