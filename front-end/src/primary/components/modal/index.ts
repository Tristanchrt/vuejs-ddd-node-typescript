import ModalComponent from '@/primary/components/modal/Modal.component';
import ModalVue from '@/primary/components/modal/Modal.vue';

export { ModalComponent, ModalVue };
