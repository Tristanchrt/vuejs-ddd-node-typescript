import { Component, Inject, Prop, Vue, Watch } from 'vue-property-decorator';

export default class HeaderArticleComponent extends Vue {
  isMenuDisplayed: boolean = false;

  toggleMenu = () => {
    this.isMenuDisplayed = !this.isMenuDisplayed;
    this.$forceUpdate();
  };
  closeResponsiveMenu = () => {
    this.isMenuDisplayed = false;
    this.$forceUpdate();
  };
}
