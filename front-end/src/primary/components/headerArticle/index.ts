import HeaderArticleComponent from '@/primary/components/headerArticle/HeaderArticle.component';
import HeaderArticleVue from '@/primary/components/headerArticle/HeaderArticle.vue';

export { HeaderArticleComponent, HeaderArticleVue };
