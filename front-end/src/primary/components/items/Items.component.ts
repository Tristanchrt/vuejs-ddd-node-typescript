import { Component, Vue, Prop, Inject } from 'vue-property-decorator';

import { ItemRepository } from '../../../domain/item/ItemRepository';

import { ItemVue } from './item/index';

import { Item } from '@/domain/item/Item';
import { StoreRepository } from '@/domain/store/StoreRepository';

@Component({
  components: {
    ItemVue,
  },
})
export default class ItemsComponent extends Vue {
  @Inject()
  private storeRepository!: () => StoreRepository;

  @Inject()
  private itemRepository!: () => ItemRepository; // TODO FOR TEST FAIRE COMME LE STORE POUR LES TESTS AVEC FIXTURES

  items: Array<Item> = new Array<Item>();

  selectChat = (id: any): void => {
    this.storeRepository()
      .user()
      .setToken(id);
  };

  created() {
    this.itemRepository()
      .getItems()
      .then(result => (this.items = result))
      .catch();
  }
}
