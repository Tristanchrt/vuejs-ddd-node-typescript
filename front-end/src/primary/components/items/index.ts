import ItemsComponent from '@/primary/components/items/Items.component';
import ItemsVue from '@/primary/components/items/Items.vue';

export { ItemsComponent, ItemsVue };
