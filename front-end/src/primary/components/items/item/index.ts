import ItemComponent from '@/primary/components/items/item/Item.component';
import ItemVue from '@/primary/components/items/item/Item.vue';

export { ItemComponent, ItemVue };
