import { Component, Vue, Prop, Watch, Inject } from 'vue-property-decorator';

import { ModalVue } from '../../modal';

import { Item } from '@/domain/item/Item';
import { StoreRepository } from '@/domain/store/StoreRepository';

@Component({
  components: {
    ModalVue,
  },
})
export default class ItemComponent extends Vue {
  @Prop({ type: Object, required: true })
  item!: Item;
  @Inject()
  private storeRepository!: () => StoreRepository;

  modal: any | null = null;

  openModal() {
    this.storeRepository()
      .modal()
      .open({ component: ModalVue, props: { item: this.item } });
    this.$watch(
      () =>
        this.storeRepository()
          .modal()
          .getModal(),
      () =>
        (this.modal = this.storeRepository()
          .modal()
          .getModal())
    );
  }
}
