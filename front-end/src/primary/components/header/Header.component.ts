import { Component, Vue } from 'vue-property-decorator';

import { InputHeaderVue } from './inputHeader';

@Component({
  components: {
    InputHeaderVue,
  },
})
export default class HeaderComponent extends Vue {}
