import InputHeaderComponent from '@/primary/components/header/inputHeader/InputHeader.component';
import InputHeaderVue from '@/primary/components/header/inputHeader/InputHeader.vue';

export { InputHeaderComponent, InputHeaderVue };
