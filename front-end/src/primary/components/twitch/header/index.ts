import HeaderComponent from '@/primary/components/twitch/header/Header.component';
import HeaderVue from '@/primary/components/twitch/header/Header.vue';

export { HeaderComponent, HeaderVue };
