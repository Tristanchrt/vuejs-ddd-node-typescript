import { Component, Inject, Prop, Vue, Watch } from 'vue-property-decorator';

import ToolTipComponent from './ToolTip.vue';

@Component({
  components: {
    ToolTipComponent,
  },
})
export default class HeaderComponent extends Vue {}
