import MenuChannelsComponent from '@/primary/components/twitch/menuChannels/MenuChannels.component';
import MenuChannelsVue from '@/primary/components/twitch/menuChannels/MenuChannels.vue';

export { MenuChannelsComponent, MenuChannelsVue };
