import SliderComponent from '@/primary/components/slider/Slider.component';
import SliderVue from '@/primary/components/slider/Slider.vue';

export { SliderComponent, SliderVue };
