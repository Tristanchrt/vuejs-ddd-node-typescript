import { Component, Inject, Prop, Vue, Watch } from 'vue-property-decorator';

import { ModalVue } from '../modal';

import { StoreRepository } from '@/domain/store/StoreRepository';

@Component({})
export default class SliderComponent extends Vue {
  images: Array<string> = [
    'https://cdn.pixabay.com/photo/2015/12/12/15/24/amsterdam-1089646_1280.jpg',
    'https://cdn.pixabay.com/photo/2016/02/17/23/03/usa-1206240_1280.jpg',
    'https://cdn.pixabay.com/photo/2015/05/15/14/27/eiffel-tower-768501_1280.jpg',
    'https://cdn.pixabay.com/photo/2016/12/04/19/30/berlin-cathedral-1882397_1280.jpg',
  ];
  currentIndex = 2;

  next() {
    if (this.images.length > this.currentIndex) {
      this.currentIndex += 1;
    }
  }
  prev() {
    if (this.currentIndex > 0) {
      this.currentIndex -= 1;
    }
  }
  get currentImg(): string {
    return this.images[this.currentIndex];
  }
}
