import Vue from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';

import { HeaderVue } from './primary/components/header';
import { HeaderArticleVue } from './primary/components/headerArticle';
import { ArticlePageVue } from './primary/views/articlePage';
import { HomePageVue } from './primary/views/homePage';
import { TwitchPageVue } from './primary/views/twitchPage';

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'Home',
    components: {
      header: HeaderVue,
      default: HomePageVue,
    },
  },
  {
    path: '/article',
    name: 'Article',
    components: {
      default: ArticlePageVue,
    },
  },
  {
    path: '/twitch',
    name: 'Twitch',
    components: {
      default: TwitchPageVue,
    },
  },
];

const router = new VueRouter({
  routes,
});

export default router;
