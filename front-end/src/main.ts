import axios, { AxiosInstance } from 'axios';
import Vue from 'vue';

import App from './primary/app/App.vue';
import router from './routes';
import RestItemRepository from './secondary/restItem/RestItemRepository';
import RestUserRepository from './secondary/restUser/RestUserRepository';
import RestStoreRepository from './secondary/store/RestStoreRepository';

Vue.config.productionTip = false;

const BACKEND_URL: string = window.location.protocol + '//' + window.location.hostname + ':4000';
const axiosInstance: AxiosInstance = axios.create({
  baseURL: BACKEND_URL + '/api',
});

const store = new RestStoreRepository();
const itemRepository = new RestItemRepository(axiosInstance);
const userRepository = new RestUserRepository(store, axiosInstance);

new Vue({
  router,
  render: h => h(App),
  provide: {
    storeRepository: () => store,
    itemRepository: () => itemRepository,
    userRepository: () => userRepository,
  },
}).$mount('#app');
